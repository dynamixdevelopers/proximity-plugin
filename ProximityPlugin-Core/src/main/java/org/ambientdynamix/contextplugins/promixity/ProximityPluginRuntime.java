/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.promixity;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import org.ambientdynamix.api.contextplugin.ContextListenerInformation;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;

import java.util.ArrayList;
import java.util.UUID;

/**
 * The plug-in enables apps to detect the proximity of an object from the phone screen.
 *
 * @author Shivam Verma
 */
public class ProximityPluginRuntime extends ContextPluginRuntime {
    private final String TAG = this.getClass().getSimpleName();
    private Context context;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private ArrayList<UUID> proximityListeners;

    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        this.setPowerScheme(powerScheme);
        this.context = this.getSecuredContext();
        this.proximityListeners = new ArrayList<UUID>();
    }

    @Override
    public void start() {
        Log.d(TAG, "Started!");
        mSensorManager = (SensorManager) context.getSystemService(Activity.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }

    @Override
    public void stop() {
        try {
            if (listener != null)
                mSensorManager.unregisterListener(listener);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Stopped!");
    }


    @Override
    public void destroy() {
        this.stop();
        context = null;
        Log.d(TAG, "Destroyed!");
    }

    SensorEventListener listener;

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {
        if (contextType.equals(ProximitySensorEvent.CONTEXT_TYPE)) {
            if (listener == null) {
                listener = new SensorEventListener() {
                    @Override
                    public void onSensorChanged(SensorEvent event) {
                        Log.d(TAG, "Accuracy : " + event.accuracy);
                        Log.d(TAG, "Proximity :" + event.values[0]);

                        for (UUID uuid : proximityListeners) {
                            sendContextEvent(uuid, new ProximitySensorEvent(event.accuracy, event.values[0]));
                        }
                    }

                    @Override
                    public void onAccuracyChanged(Sensor sensor, int accuracy) {

                    }
                };
                mSensorManager.registerListener(listener, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
            sendContextRequestSuccess(requestId);
        }
    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {
        // Warn that we don't handle configured requests
        Log.w(TAG, "handleConfiguredContextRequest called, but we don't support configuration!");
        // Drop the config and default to handleContextRequest
        handleContextRequest(requestId, contextType);
    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // Not supported
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {
        // Not supported
    }


    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        proximityListeners.add(listenerInfo.getListenerId());
        return true;
    }

}