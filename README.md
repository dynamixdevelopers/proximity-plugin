# Welcome
This README provides an overview of the `org.ambientdynamix.contextplugins.promixity` plug-in for the [Dynamix Framework](http://ambientdynamix.org). It allows apps to determine how far away a physical object is from the face of the handset device. Most proximity sensors return the absolute distance, in cm, but some return only near and far values. 

Note that this guide assumes an understanding of the [Dynamix Framework documentation](http://ambientdynamix.org/documentation/).

# Plug-in Requirements
* Dynamix version 2.1 and higher

# Context Support
* `org.ambientdynamix.contextplugins.promixity.subscribe` subscribe to proximity value changes. 

# Native App Usage
Integrate the Dynamix Framework within your app as [shown here](http://ambientdynamix.org/documentation/integration/). This guide demonstrates how to connect to Dynamix, create context handlers, register for context support, receive events, interact with plug-ins, and use callbacks. To view a simple example of using Dynamix in your native programs, see [this guide](http://ambientdynamix.org/documentation/native-app-quickstart).

## Add context support for the plug-in.
```
#!java
contextHandler.addContextSupport("org.ambientdynamix.contextplugins.promixity", "org.ambientdynamix.contextplugins.promixity.subscribe", callback, listener);
```
## Handle Events
Once context support is successfully added, the supplied listener will start receiving the proximity change events. The events are of type `ProximitySensorEvent`, which includes a `proximity` value along with the `accuracy` . 

Apps can make use of the `getAccuracy` and `getProximity` methods of the `ProximitySensorEvent` class to retrieve these values. 








